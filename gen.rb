require 'erb'

# List directories only
DIRECTORIES = Dir.glob('*/').map { |dir| dir.chomp('/') }

# Read the ERB template for main .gitlab-ci.yml
main_template_str = File.read('gitlab-ci.yml.erb')
main_template = ERB.new(main_template_str)
File.write('.gitlab-ci.yml', main_template.result(binding))

# Read the ERB template for sub-directory .gitlab-ci.yml files
sub_template_str = File.read('sub_gitlab-ci.yml.erb')
sub_template = ERB.new(sub_template_str)

DIRECTORIES.each do |dir|
  File.write("#{dir}/.gitlab-ci.yml", sub_template.result(binding))
end
